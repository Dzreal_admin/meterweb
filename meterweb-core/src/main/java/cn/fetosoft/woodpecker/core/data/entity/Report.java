package cn.fetosoft.woodpecker.core.data.entity;

import cn.fetosoft.woodpecker.core.enums.DateFormatEnum;
import cn.fetosoft.woodpecker.core.util.NumberFormatUtil;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.text.NumberFormat;

/**
 * 聚合报表
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/7/1 10:34
 */
@Setter
@Getter
@Document("wp_report")
public class Report extends BaseElement {

	private String threadId;

	private String elementId;

	private long samples;

	private double average;

	private Number median;

	private Number percent90;

	private Number percent95;

	private Number percent99;

	private Number min;

	private Number max;

	private double errorRate;

	private double throughput;

	private String throughputFmt;

	private double sendBytes;

	private String sendBytesFmt;

	private double receiveBytes;

	private String receiveBytesFmt;

	private String createTimeFmt;

	public String getThroughputFmt() {
		if(throughput>0){
			throughputFmt = NumberFormatUtil.doubleFormat(throughput, 4);
		}
		return throughputFmt;
	}

	public String getSendBytesFmt() {
		if(sendBytes>0){
			sendBytesFmt = NumberFormatUtil.doubleFormat(sendBytes, 4);
		}
		return sendBytesFmt;
	}

	public String getReceiveBytesFmt() {
		if(receiveBytes>0){
			receiveBytesFmt = NumberFormatUtil.doubleFormat(receiveBytes, 4);
		}
		return receiveBytesFmt;
	}

	public String getCreateTimeFmt() {
		if(this.getCreateTime()!=null){
			createTimeFmt = DateFormatEnum.YMD_HMS_SSS.timestampToString(this.getCreateTime());
		}
		return createTimeFmt;
	}
}
