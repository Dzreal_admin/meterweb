package cn.fetosoft.woodpecker.core.pubsub;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;

/**
 * 发布指令
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/9/12 21:53
 */
@Setter
@Getter
public class Command {

    public static final String CMD_ENABLED = "enabled";
    public static final String CMD_START_TEST = "startTest";

    private String command;

    private String planId;

    private String userId;

    private String testId;

    private String clusterId;

    private Boolean enabled;

    private long cmdTime;

    @Override
    public String toString(){
        return JSON.toJSONString(this);
    }
}
