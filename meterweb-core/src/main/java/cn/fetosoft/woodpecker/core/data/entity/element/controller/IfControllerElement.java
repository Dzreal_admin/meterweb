package cn.fetosoft.woodpecker.core.data.entity.element.controller;

import cn.fetosoft.woodpecker.core.data.base.DataField;
import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * IF控制器
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/7/12 20:56
 */
@Setter
@Getter
@Document("wp_element")
public class IfControllerElement extends BaseElement {

    @DataField
    private String condition;

    @DataField
    private String evaluateAll;

    @DataField
    private String useExpression;
}
