package cn.fetosoft.woodpecker.core.jmeter.element.processor;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.processor.RegexExtractorElement;
import cn.fetosoft.woodpecker.core.jmeter.element.AbstractElementBuild;
import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.extractor.RegexExtractor;
import org.apache.jmeter.testelement.TestElement;
import org.springframework.stereotype.Component;

/**
 * 创建正则表达式提取器
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/7/21 16:29
 */
@Component("regexExtractorImpl")
public class CreateRegexExtractorImpl extends AbstractElementBuild {

	@Override
	protected TestElement convert(BaseElement be) throws Exception {
		RegexExtractorElement element = (RegexExtractorElement) be;
		RegexExtractor regex = new RegexExtractor();
		if(SCOPE_ALL.equalsIgnoreCase(element.getScope())){
			regex.setScopeAll();
		}else if(SCOPE_PARENT.equalsIgnoreCase(element.getScope())){
			regex.setScopeParent();
		}else if(SCOPE_CHILDREN.equalsIgnoreCase(element.getScope())){
			regex.setScopeChildren();
		}else if(SCOPE_VARIABLE.equalsIgnoreCase(element.getScope())){
			regex.setScopeVariable(element.getScopeVariable());
		}
		regex.setRefName(element.getRefname());
		regex.setRegex(element.getRegex());
		regex.setTemplate(element.getTemplate());
		regex.setDefaultValue(element.getDefaultValue());
		regex.setMatchNumber(element.getMatchNumber());
		regex.setUseField(element.getUseField());
		if(StringUtils.isBlank(element.getDefaultEmptyValue())){
			regex.setDefaultEmptyValue(false);
			regex.setDefaultValue(element.getDefaultValue());
		}else{
			regex.setDefaultEmptyValue(true);
			regex.setDefaultValue(StringUtils.EMPTY);
		}
		return regex;
	}
}
