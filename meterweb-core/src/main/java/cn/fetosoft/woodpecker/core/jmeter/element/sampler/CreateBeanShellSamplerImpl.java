package cn.fetosoft.woodpecker.core.jmeter.element.sampler;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.sampler.BeanShellSamplerElement;
import cn.fetosoft.woodpecker.core.jmeter.element.AbstractElementBuild;
import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.protocol.java.sampler.BeanShellSampler;
import org.apache.jmeter.testelement.TestElement;
import org.springframework.stereotype.Component;

/**
 * 创建BeanShellSampler
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/9/5 19:19
 */
@Component("beanShellSamplerImpl")
public class CreateBeanShellSamplerImpl extends AbstractElementBuild {

    @Override
    protected TestElement convert(BaseElement be) throws Exception {
        BeanShellSamplerElement element = (BeanShellSamplerElement) be;
        BeanShellSampler sampler = new BeanShellSampler();
        sampler.setProperty(BeanShellSampler.SCRIPT, element.getScript());
        boolean reset = false;
        if(StringUtils.isNotBlank(element.getResetInterpreter())){
            reset = Boolean.parseBoolean(element.getResetInterpreter());
        }
        sampler.setProperty(BeanShellSampler.RESET_INTERPRETER, reset);
        sampler.setProperty(BeanShellSampler.PARAMETERS, element.getParameters());
        sampler.setProperty(BeanShellSampler.FILENAME, element.getFilename());
        return sampler;
    }
}
