package cn.fetosoft.woodpecker.core.jmeter.element.config;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.config.DataSourceConfigElement;
import cn.fetosoft.woodpecker.core.jmeter.element.AbstractElementBuild;
import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.protocol.jdbc.config.DataSourceElement;
import org.apache.jmeter.testelement.TestElement;
import org.springframework.stereotype.Component;

/**
 * 创建数据源配置
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/8/2 14:33
 */
@Component("dataSourceConfigImpl")
public class CreateDataSourceConfigImpl extends AbstractElementBuild {

	@Override
	protected TestElement convert(BaseElement be) throws Exception {
		DataSourceConfigElement element = (DataSourceConfigElement) be;
		DataSourceElement config = new DataSourceElement();
		config.setProperty("dataSource", element.getDataSource());
		config.setProperty("poolMax", element.getPoolMax());
		config.setProperty("timeout", element.getTimeout());
		config.setProperty("trimInterval", element.getTrimInterval());
		if(StringUtils.isNotBlank(element.getAutocommit())){
			config.setProperty("autocommit", Boolean.parseBoolean(element.getAutocommit()));
		}
		config.setProperty("transactionIsolation", element.getTransactionIsolation());
		if(StringUtils.isNotBlank(element.getPreinit())) {
			config.setProperty("preinit", Boolean.parseBoolean(element.getPreinit()));
		}
		config.setProperty("initQuery", element.getInitQuery());
		if(StringUtils.isNotBlank(element.getKeepAlive())){
			config.setProperty("keepAlive", Boolean.parseBoolean(element.getKeepAlive()));
		}
		config.setProperty("connectionAge", element.getConnectionAge());
		config.setProperty("checkQuery", element.getCheckQuery());
		config.setProperty("dbUrl", element.getDbUrl());
		config.setProperty("driver", element.getDriver());
		config.setProperty("username", element.getUsername());
		config.setProperty("password", element.getPassword());
		config.setProperty("connectionProperties", element.getConnectionProperties());
		return config;
	}
}
