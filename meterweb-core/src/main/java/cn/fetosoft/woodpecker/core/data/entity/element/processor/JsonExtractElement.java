package cn.fetosoft.woodpecker.core.data.entity.element.processor;

import cn.fetosoft.woodpecker.core.data.base.DataField;
import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * JSON提取器
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/26 16:24
 */
@Setter
@Getter
@Document("wp_element")
public class JsonExtractElement extends BaseElement {

	@DataField
	private String referenceNames;

	@DataField
	private String jsonPathExprs;

	@DataField
	private String matchNumbers;

	@DataField
	private String defaultValues;
}
