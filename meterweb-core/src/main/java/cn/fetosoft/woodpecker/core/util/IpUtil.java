package cn.fetosoft.woodpecker.core.util;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/9/12 15:54
 */
public final class IpUtil {

    /**
     * 获取本机IP地址
     * @author guobingbing
     * @date 2020/1/28 18:58
     * @return list
     */
    public static List<String> getLocalIP() {
        List<String> hosts = new ArrayList<>();
        try{
            Enumeration allNetInterfaces = NetworkInterface.getNetworkInterfaces();
            while (allNetInterfaces.hasMoreElements()){
                NetworkInterface netInterface = (NetworkInterface) allNetInterfaces.nextElement();
                String displayName = netInterface.getDisplayName().toLowerCase();
                String name = netInterface.getName();
                boolean isEth = (name.startsWith("eth") || name.startsWith("wlan")) && !displayName.contains("virtual") && !name.contains("docker");
                if(isEth) {
                    Enumeration addresses = netInterface.getInetAddresses();
                    while (addresses.hasMoreElements()) {
                        InetAddress ip = (InetAddress) addresses.nextElement();
                        if (ip != null && ip instanceof Inet4Address) {
                            String localHost = ip.getHostAddress();
                            if (!localHost.trim().startsWith("127")) {
                                hosts.add(localHost);
                            }
                        }
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return hosts;
    }

    /**
     * 获取单一IP
     * @return
     */
    public static String getSingleLocalIP() {
        String host = "";
        try {
            host = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return host;
    }

    /**
     * 将ip转换成long型数值
     * @author guobingbing
     * @date 2020/1/28 18:58
     * @param ip
     * @return long
     */
    public static long ipToLong(String ip){
        long iplong = 0;
        String[] ipArr = ip.split("\\.");
        for(int i=0; i<ipArr.length; i++){
            long tmp = Long.parseLong(ipArr[i]);
            iplong += tmp << ((ipArr.length-1-i)*8);
        }
        return iplong;
    }

    /**
     * 将long型数值转换成ip串
     * @author guobingbing
     * @date 2020/1/28 18:58
     * @param ip
     * @return java.lang.String
     */
    public static String longToIp(long ip){
        StringBuilder sb = new StringBuilder();
        sb.append((ip >> 24) & 0xFF).append(".");
        sb.append((ip >> 16) & 0xFF).append(".");
        sb.append((ip >> 8) & 0xFF).append(".");
        sb.append(ip & 0xFF);
        return sb.toString();
    }
}
