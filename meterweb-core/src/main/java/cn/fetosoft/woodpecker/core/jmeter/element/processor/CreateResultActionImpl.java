package cn.fetosoft.woodpecker.core.jmeter.element.processor;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.processor.ResultActionElement;
import cn.fetosoft.woodpecker.core.jmeter.element.AbstractElementBuild;
import org.apache.jmeter.reporters.ResultAction;
import org.apache.jmeter.testelement.TestElement;
import org.springframework.stereotype.Component;

/**
 * 创建状态处理器
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/9/12 9:40
 */
@Component("resultActionImpl")
public class CreateResultActionImpl extends AbstractElementBuild {

    @Override
    protected TestElement convert(BaseElement be) throws Exception {
        ResultActionElement element = (ResultActionElement) be;
        ResultAction processor = new ResultAction();
        processor.setErrorAction(element.getAction());
        return processor;
    }
}
