package cn.fetosoft.woodpecker.core.data.entity.element.assertion;

import cn.fetosoft.woodpecker.core.data.base.DataField;
import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Json断言
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/8/16 14:01
 */
@Setter
@Getter
@Document("wp_element")
public class JsonPathAssertionElement extends BaseElement {

	@DataField
	private String jsonPath;
	@DataField
	private String expectedValue;
	@DataField
	private String jsonValidation;
	@DataField
	private String expectNull;
	@DataField
	private String invert;
	@DataField
	private String isRegex;
}
