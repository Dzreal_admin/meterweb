package cn.fetosoft.woodpecker.core.jmeter.element.processor;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.processor.JdbcPostElement;
import cn.fetosoft.woodpecker.core.jmeter.element.AbstractElementBuild;
import org.apache.jmeter.protocol.jdbc.processor.JDBCPostProcessor;
import org.apache.jmeter.testelement.TestElement;
import org.springframework.stereotype.Component;

/**
 * 创建后置处理器
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/9/6 21:24
 */
@Component("jdbcPostImpl")
public class CreateJdbcPostProcessorImpl extends AbstractElementBuild {

    @Override
    protected TestElement convert(BaseElement be) throws Exception {
        JdbcPostElement element = (JdbcPostElement) be;
        JDBCPostProcessor processor = new JDBCPostProcessor();
        processor.setProperty("dataSource", element.getDataSource());
        processor.setProperty("queryType", element.getQueryType());
        processor.setProperty("query", element.getQuery());
        processor.setProperty("queryArguments", element.getQueryArguments());
        processor.setProperty("queryArgumentsTypes", element.getQueryArgumentsTypes());
        processor.setProperty("variableNames", element.getVariableNames());
        processor.setProperty("resultVariable", element.getResultVariable());
        processor.setProperty("queryTimeout", element.getQueryTimeout());
        processor.setProperty("resultSetMaxRows", element.getResultSetMaxRows());
        processor.setProperty("resultSetHandler", element.getResultSetHandler());
        return processor;
    }
}
