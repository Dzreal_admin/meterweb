package cn.fetosoft.woodpecker.core.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;

/**
 * @author guobingbing
 * @create 2020-03-07 19:29
 */
public class MD5Util {

	private final static String ENCODING = "utf-8";
	private static final Logger logger = LoggerFactory.getLogger(MD5Util.class);

	/**
	 * 32位MD5加密
	 * @Author guobingbing
	 * @Date 2016/12/23 11:08
	 * @param source
	 * @return
	 */
	public static String md5(String source) {
		StringBuffer sb = new StringBuffer(32);
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] array = md.digest(source.getBytes(ENCODING));
			for (int i = 0; i < array.length; i++) {
				sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100)
						.substring(1, 3));
			}
		} catch (Exception e) {
			logger.error("md5 error : " ,e);
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		//840e345e3d8c9fcd51aef857e4f7eefa
		System.out.println(md5("admin888"));
	}
}
