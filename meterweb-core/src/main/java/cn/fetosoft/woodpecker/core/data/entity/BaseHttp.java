package cn.fetosoft.woodpecker.core.data.entity;

import cn.fetosoft.woodpecker.core.data.base.DataField;
import lombok.Getter;
import lombok.Setter;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/10 17:41
 */
@Setter
@Getter
public class BaseHttp extends BaseElement {

	@DataField
	private String protocol;

	@DataField
	private String domain;

	@DataField
	private String port;

	@DataField
	private String path;

	@DataField
	private String contentEncode;

	@DataField
	private String connectTimeout;

	@DataField
	private String responseTimeout;
}
