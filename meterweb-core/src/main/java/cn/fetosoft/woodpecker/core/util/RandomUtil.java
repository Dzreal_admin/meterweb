package cn.fetosoft.woodpecker.core.util;

import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author guobingbing
 * @create 2020-02-20 21:06
 */
public class RandomUtil {

	public static String uuid(){
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

	/**
	 * 获取一定长度的随机数
	 * @author guobingbing
	 * @date 2018/9/5 20:18
	 * @param length
	 * @return java.lang.String
	 */
	public static String getLenRandom(int length){
		int random = ThreadLocalRandom.current().nextInt((int)Math.pow(10, length));
		String incStr = String.valueOf(random);
		if(incStr.length()<length){
			return String.format("%0"+length+"d", random);
		}else {
			return incStr;
		}
	}

	/**
	 * 格式化数字，位数不足则补0
	 * @param seq 基数
	 * @param len 数字位数
	 * @return
	 */
	public static String mixLeng(long seq, long len){
		String seqNo = "";
		if(seq<len){
			int minLen = String.valueOf(len).length();
			seqNo = String.format("%0"+minLen+"d", seq);
		}else{
			seqNo = String.valueOf(seq);
		}
		return seqNo;
	}

	/**
	 * 获取随机数字串
	 * @author guobingbing
	 * @wechat t_gbinb
	 * @date 2021/1/25 13:44
	 * @param len
	 * @return java.lang.String
	 * @version 1.0
	 */
	public static String getRandomNumber(int len){
		String str = "67890123451234567890";
		return getRandomChar(str, len);
	}

	/**
	 * 获取随机字母数字串
	 * @author guobingbing
	 * @wechat t_gbinb
	 * @date 2021/1/25 13:44
	 * @param len
	 * @return java.lang.String
	 * @version 1.0
	 */
	public static String getRandomNumChar(int len){
		String str = "ABCDEFG678abcdnopqr90HIJKLMN12345PQRSTU12efjhigklm345VWXYZ67890stuvwxyz";
		return getRandomChar(str, len);
	}

	/**
	 * 获取随机字符串，包含特殊字符
	 * @author guobingbing
	 * @wechat t_gbinb
	 * @date 2021/4/8 9:07
	 * @param len
	 * @return java.lang.String
	 * @version 1.0
	 */
	public static String getRandomKey(int len){
		String str = "ABCDEFG678=-+abcdnopqr90HIJKLMN12345PQRS=-+/TU12efjhigklm345VWXYZ67890stuvwxyz=-+";
		return getRandomChar(str, len);
	}

	/**
	 * 产生随机串
	 * @param randomChar
	 * @param len
	 * @return
	 */
	private static String getRandomChar(String randomChar, int len){
		char[] chars = randomChar.toCharArray();
		StringBuilder sb = new StringBuilder();
		ThreadLocalRandom random = ThreadLocalRandom.current();
		for(int i=0; i< len; i++){
			int ram = random.nextInt(chars.length);
			char c = chars[ram];
			sb.append(c);
		}
		return sb.toString();
	}
}
