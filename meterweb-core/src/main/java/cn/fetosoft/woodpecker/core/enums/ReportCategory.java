package cn.fetosoft.woodpecker.core.enums;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/7/1 16:28
 */
public enum ReportCategory {

	/**
	 * Aggregate Report
	 */
	AggregateReport("aggregateReport", "聚合报告");

	private String name;
	private String text;

	ReportCategory(String name, String text){
		this.name = name;
		this.text = text;
	}

	public String getName(){
		return this.name;
	}

	public String getText(){
		return this.text;
	}
}
