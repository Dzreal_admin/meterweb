package cn.fetosoft.woodpecker.core.util;

import lombok.Getter;
import lombok.Setter;

/**
 * 上传文件信息
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/7/22 11:25
 */
@Setter
@Getter
public class UploadFile {

	private String name;

	private String extension;

	private String absolutePath;

	private String relativePath;

	private String fileUrl;
}
