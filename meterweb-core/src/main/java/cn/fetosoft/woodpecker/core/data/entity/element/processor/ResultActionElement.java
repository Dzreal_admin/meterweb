package cn.fetosoft.woodpecker.core.data.entity.element.processor;

import cn.fetosoft.woodpecker.core.data.base.DataField;
import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 结果状态处理器
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/9/12 9:38
 */
@Setter
@Getter
@Document("wp_element")
public class ResultActionElement extends BaseElement {

    @DataField
    private int action;
}
