package cn.fetosoft.woodpecker.core.jmeter;

import cn.fetosoft.woodpecker.core.event.TestPlanEvent;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.testelement.TestPlan;
import org.apache.jmeter.threads.JMeterVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/24 17:39
 */
public class TestPlanExtend extends TestPlan {

	private static final Logger logger = LoggerFactory.getLogger(TestPlanExtend.class);
	private ApplicationContext applicationContext;

	public TestPlanExtend(ApplicationContext applicationContext){
		super();
		this.applicationContext = applicationContext;
	}

	private String planId;
	private String userId;
	private String testId;

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getTestId() {
		return testId;
	}

	public void setTestId(String testId) {
		this.testId = testId;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void testEnded() {
		super.testEnded();
		JMeterVariables variables = this.getThreadContext().getVariables();
		variables.remove(HTTPSamplerBase.HEADER_MANAGER);
		variables.remove(HTTPSamplerBase.DEFAULT_CONFIG);
		System.out.println("测试结束=========================" + this.getTestId());
		System.out.println(this.getUserId());
		System.out.println(this.getPlanId());

		TestPlanEvent event = new TestPlanEvent(getName());
		event.setPlanId(this.getPlanId());
		event.setTestId(this.getTestId());
		event.setUserId(this.getUserId());
		this.applicationContext.publishEvent(event);
	}
}
