package cn.fetosoft.woodpecker.core.data.entity.element.assertion;

import cn.fetosoft.woodpecker.core.data.base.DataField;
import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 响应断言
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/8/15 16:48
 */
@Setter
@Getter
@Document("wp_element")
public class ResponseAssertionElement extends BaseElement {

    @DataField
    private String scope = "parent";
    @DataField
    private String scopeVariable;
    @DataField
    private String testField = "Assertion.response_data";
    @DataField
    private String assumeSuccess;
    @DataField
    private String testRule = "16";
    @DataField
    private String testNo;
    @DataField
    private String testOr;
    @DataField
    private String customMessage;
    @DataField
    private String testStrings;
}
