package cn.fetosoft.woodpecker.core.util;

import cn.fetosoft.woodpecker.core.data.entity.element.ThreadGroupElement;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/18 15:21
 */
public abstract class BeanUtil {

	public static List<Field> getDeclaredFields(Class clazz){
		List<Field> fields = new ArrayList<>(64);
		while (clazz!=Object.class){
			for (Field field : clazz.getDeclaredFields()) {
				fields.add(field);
			}
			clazz = clazz.getSuperclass();
		}
		return fields;
	}
}
