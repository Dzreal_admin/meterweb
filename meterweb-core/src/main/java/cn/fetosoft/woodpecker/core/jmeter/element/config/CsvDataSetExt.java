package cn.fetosoft.woodpecker.core.jmeter.element.config;

import org.apache.jmeter.config.CSVDataSet;
import org.apache.jmeter.testelement.property.BooleanProperty;
import org.apache.jmeter.testelement.property.StringProperty;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/7/23 10:49
 */
public class CsvDataSetExt extends CSVDataSet {

	private static final String FILENAME = "filename";
	private static final String FILE_ENCODING = "fileEncoding";
	private static final String VARIABLE_NAMES = "variableNames";
	private static final String IGNORE_FIRST_LINE = "ignoreFirstLine";
	private static final String DELIMITER = "delimiter";
	private static final String RECYCLE = "recycle";
	private static final String STOPTHREAD = "stopThread";
	private static final String QUOTED_DATA = "quotedData";
	private static final String SHAREMODE = "shareMode";

	public void initProperties(){
		this.setProperty(new StringProperty(FILENAME, this.getFilename()));
		this.setProperty(new StringProperty(FILE_ENCODING, this.getFileEncoding()));
		this.setProperty(new StringProperty(VARIABLE_NAMES, this.getVariableNames()));
		this.setProperty(new BooleanProperty(IGNORE_FIRST_LINE, this.isIgnoreFirstLine()));
		this.setProperty(new StringProperty(DELIMITER, this.getDelimiter()));
		this.setProperty(new BooleanProperty(QUOTED_DATA, this.getQuotedData()));
		this.setProperty(new BooleanProperty(RECYCLE, this.getRecycle()));
		this.setProperty(new StringProperty(SHAREMODE, this.getShareMode()));
		this.setProperty(new BooleanProperty(STOPTHREAD, this.getStopThread()));
	}
}
