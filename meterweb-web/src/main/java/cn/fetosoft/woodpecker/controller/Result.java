package cn.fetosoft.woodpecker.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.Setter;
import java.util.List;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/8 15:15
 */
@Setter
@Getter
public final class Result<T> {

	@JSONField(serialize = false)
	public static final int SUCCESS = 1;
	@JSONField(serialize = false)
	public static final int ERROR = 0;
	@JSONField(serialize = false)
	public static final int LOGIN = 2;

	private int status = SUCCESS;

	private String errorMsg;

	private long total;

	private List<T> list;

	private T obj;

	private Result(int status){
		this.status = status;
	}

	public static <T> Result<T> successResult(){
		return new Result<>(SUCCESS);
	}

	public static <T> Result<T> errorResult(){
		return new Result<>(ERROR);
	}

	public static <T> Result<T> create(int status){
		return new Result<>(status);
	}

	@Override
	public String toString(){
		return JSON.toJSONString(this);
	}
}
