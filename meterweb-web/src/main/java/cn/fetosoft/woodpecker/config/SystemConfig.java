package cn.fetosoft.woodpecker.config;

import cn.fetosoft.woodpecker.core.pubsub.cluster.ClusterPublication;
import cn.fetosoft.woodpecker.core.pubsub.cluster.ClusterSubscription;
import cn.fetosoft.woodpecker.core.pubsub.zookeeper.CuratorFrameworkBuilder;
import cn.fetosoft.woodpecker.core.pubsub.zookeeper.ZookeeperCfg;
import lombok.Getter;
import lombok.Setter;
import org.apache.catalina.Context;
import org.apache.curator.framework.CuratorFramework;
import org.apache.tomcat.util.scan.StandardJarScanner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/8 15:00
 */
@Setter
@Getter
@Configuration
public class SystemConfig {

	@Bean
	public TomcatServletWebServerFactory createTomcatFactory(){
		return new TomcatServletWebServerFactory(){
			@Override
			protected void postProcessContext(Context context) {
				((StandardJarScanner)context.getJarScanner()).setScanManifest(false);
			}
		};
	}

	@Bean
	public ServerEndpointExporter serverEndpointExporter() {
		return new ServerEndpointExporter();
	}

	@Bean
	@ConfigurationProperties(prefix = "meter.zookeeper")
	public ZookeeperCfg createZookeeperCfg(){
		return new ZookeeperCfg();
	}

	@Bean
	@ConditionalOnBean({ZookeeperCfg.class})
	public CuratorFramework createCuratorFramework(ZookeeperCfg cfg){
		return new CuratorFrameworkBuilder(cfg).createFramework();
	}

	@Bean
	public ClusterSubscription createSubscription(){
		return new ClusterSubscription();
	}

	@Bean
	public ClusterPublication createPublication(ZookeeperCfg cfg){
		return new ClusterPublication(cfg.getSubPath());
	}
}
