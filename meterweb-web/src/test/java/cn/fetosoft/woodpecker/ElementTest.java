package cn.fetosoft.woodpecker;

import cn.fetosoft.woodpecker.core.data.base.BaseEntity;
import cn.fetosoft.woodpecker.core.data.entity.*;
import cn.fetosoft.woodpecker.core.data.entity.element.config.HttpConfigElement;
import cn.fetosoft.woodpecker.core.data.entity.element.config.HttpHeaderElement;
import cn.fetosoft.woodpecker.core.data.form.ElementForm;
import cn.fetosoft.woodpecker.core.data.service.ElementService;
import com.alibaba.fastjson.JSON;
import com.mongodb.client.result.DeleteResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/11 10:42
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ApplicationStart.class})
public class ElementTest {

	@Autowired
	private ElementService elementService;

	@Test
	public void findList(){
		ElementForm form = new ElementForm();
		form.setParentId("1");
		form.setAscField("sort");
		try {
			List<BaseElement> list = elementService.selectListByForm(form, BaseElement.class);
			if(list!=null){
				System.out.println(JSON.toJSONString(list));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void addRequestDefault() throws Exception {
		HttpConfigElement req = new HttpConfigElement();
		req.setPlanId("P001");
		req.setCategory("config");
		req.setParentId("1");
		req.setSort(1);
		req.setName("HTTP请求默认值");
		req.setDest("设置HTTP请求基本参数");
		req.setDomain("testapp.huanuo.co");
		req.setProtocol("https");
		req.setPort("443");
		BaseEntity result = elementService.insert(req);
		System.out.println(JSON.toJSONString(result));
	}

	@Test
	public void addHttpHeader() throws Exception {
		HttpHeaderElement httpHeader = new HttpHeaderElement();
		httpHeader.setPlanId("P001");
		httpHeader.setCategory("config");
		httpHeader.setParentId("1");
		httpHeader.setSort(2);
		httpHeader.setName("HTTP请求头信息");
		httpHeader.setDest("全局HTTP请求头配置信息");

		BaseEntity result = elementService.insert(httpHeader);
		System.out.println(JSON.toJSONString(result));
	}

	@Test
	public void deleteProject(){
		DeleteResult result = elementService.deleteById("60c82635fc867e3a43b348e7", HttpConfigElement.class);
		System.out.println(JSON.toJSONString(result));
	}

	@Test
	public void deleteElement(){
		DeleteResult result = elementService.deleteById("60c82664b3804416f9b7e227", HttpHeaderElement.class);
		System.out.println(JSON.toJSONString(result));
	}
}
