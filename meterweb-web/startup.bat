@echo off

setlocal
set "CURRENT_DIR=%cd%"
echo %CURRENT_DIR%
cd "%CURRENT_DIR%"

java -Xmx512m -Xms512m -jar meterweb.war
cmd /k