<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>cn.fetosoft.woodpecker</groupId>
  <artifactId>meterweb</artifactId>
  <packaging>pom</packaging>
  <version>1.0</version>
  <modules>
    <module>meterweb-core</module>
    <module>meterweb-jmeter</module>
    <module>meterweb-web</module>
    <module>meterweb-test</module>
    <module>meterweb-cluster</module>
  </modules>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <java.version>1.8</java.version>
    <spring.version>5.2.1.RELEASE</spring.version>
    <springboot.version>2.2.1.RELEASE</springboot.version>
    <mybatis-starter.version>2.1.0</mybatis-starter.version>
    <mybatis.version>3.4.0</mybatis.version>
    <mybatis-spring.version>1.3.0</mybatis-spring.version>
    <slf4j.version>1.7.30</slf4j.version>
    <logback.version>1.2.3</logback.version>
    <logback-spring.version>0.1.5</logback-spring.version>
    <fastjson.version>1.2.69</fastjson.version>
    <quartz.version>2.3.0</quartz.version>
    <curator.version>5.0.0</curator.version>
    <amazonaws.s3>1.10.77</amazonaws.s3>
    <jedis.version>3.0.1</jedis.version>
    <httpcomponents.version>4.4.12</httpcomponents.version>
    <httpclient.version>4.5.10</httpclient.version>
    <lombok.version>1.18.10</lombok.version>
    <druid.version>1.1.20</druid.version>
    <javaee-api.version>8.0</javaee-api.version>
    <servlet-api.version>4.0.1</servlet-api.version>
    <tomcat.embed.version>9.0.27</tomcat.embed.version>
    <fetosoft.commons.version>1.0-SNAPSHOT</fetosoft.commons.version>
    <google.guava.version>28.2-jre</google.guava.version>
    <google.zxing.version>3.4.0</google.zxing.version>
    <jwt.version>3.8.3</jwt.version>
    <mongodb.version>3.12.0</mongodb.version>
    <validator.version>6.0.18.Final</validator.version>
  </properties>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <scope>test</scope>
        <version>4.13</version>
        <exclusions>
          <exclusion>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-core</artifactId>
          </exclusion>
        </exclusions>
      </dependency>

      <dependency>
        <groupId>org.junit.jupiter</groupId>
        <artifactId>junit-jupiter-api</artifactId>
        <scope>test</scope>
        <version>5.7.0</version>
      </dependency>

      <dependency>
        <groupId>org.junit.jupiter</groupId>
        <artifactId>junit-jupiter-params</artifactId>
        <version>5.7.0</version>
        <scope>test</scope>
      </dependency>

      <dependency>
        <groupId>org.hamcrest</groupId>
        <artifactId>hamcrest-core</artifactId>
        <version>2.2</version>
        <scope>test</scope>
      </dependency>

      <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot</artifactId>
        <version>${springboot.version}</version>
      </dependency>

      <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter</artifactId>
        <version>${springboot.version}</version>
      </dependency>

      <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-autoconfigure</artifactId>
        <version>${springboot.version}</version>
      </dependency>

      <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-test</artifactId>
        <version>${springboot.version}</version>
        <scope>test</scope>
        <exclusions>
          <exclusion>
            <groupId>org.junit.vintage</groupId>
            <artifactId>junit-vintage-engine</artifactId>
          </exclusion>
        </exclusions>
      </dependency>

      <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
        <version>${springboot.version}</version>
      </dependency>

      <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-jdbc</artifactId>
        <version>${springboot.version}</version>
      </dependency>

      <dependency>
        <groupId>org.mybatis.spring.boot</groupId>
        <artifactId>mybatis-spring-boot-starter</artifactId>
        <version>${mybatis-starter.version}</version>
      </dependency>

      <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-tomcat</artifactId>
        <version>${springboot.version}</version>
        <scope>provided</scope>
      </dependency>

      <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-test</artifactId>
        <version>${springboot.version}</version>
      </dependency>

      <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-websocket</artifactId>
        <version>${springboot.version}</version>
      </dependency>

      <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-loader</artifactId>
        <version>${springboot.version}</version>
      </dependency>

      <!-- spring核心包 -->
      <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-context</artifactId>
        <version>${spring.version}</version>
      </dependency>

      <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-core</artifactId>
        <version>${spring.version}</version>
      </dependency>

      <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-web</artifactId>
        <version>${spring.version}</version>
      </dependency>

      <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-webmvc</artifactId>
        <version>${spring.version}</version>
      </dependency>

      <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-context-support</artifactId>
        <version>${spring.version}</version>
      </dependency>

      <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-test</artifactId>
        <version>${spring.version}</version>
      </dependency>

      <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-beans</artifactId>
        <version>${spring.version}</version>
      </dependency>

      <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-jdbc</artifactId>
        <version>${spring.version}</version>
      </dependency>

      <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-aop</artifactId>
        <version>${spring.version}</version>
      </dependency>

      <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-tx</artifactId>
        <version>${spring.version}</version>
      </dependency>

      <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-aspects</artifactId>
        <version>${spring.version}</version>
      </dependency>
      <!--Spring-->

      <!--mongoDB-->
      <dependency>
        <groupId>org.springframework.data</groupId>
        <artifactId>spring-data-mongodb</artifactId>
        <version>${springboot.version}</version>
      </dependency>

      <dependency>
        <groupId>org.mongodb</groupId>
        <artifactId>mongo-java-driver</artifactId>
        <version>${mongodb.version}</version>
      </dependency>
      <!--mongoDB-->

      <dependency>
        <groupId>javax</groupId>
        <artifactId>javaee-api</artifactId>
        <version>${javaee-api.version}</version>
        <scope>compile</scope>
      </dependency>

      <!-- log start -->
      <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-api</artifactId>
        <version>${slf4j.version}</version>
      </dependency>

      <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>jcl-over-slf4j</artifactId>
        <version>${slf4j.version}</version>
      </dependency>

      <dependency>
        <groupId>ch.qos.logback</groupId>
        <artifactId>logback-core</artifactId>
        <version>${logback.version}</version>
      </dependency>

      <dependency>
        <groupId>ch.qos.logback</groupId>
        <artifactId>logback-classic</artifactId>
        <version>${logback.version}</version>
      </dependency>

      <dependency>
        <groupId>org.logback-extensions</groupId>
        <artifactId>logback-ext-spring</artifactId>
        <version>${logback-spring.version}</version>
      </dependency>
      <!-- log end -->

      <dependency>
        <groupId>redis.clients</groupId>
        <artifactId>jedis</artifactId>
        <version>${jedis.version}</version>
      </dependency>

      <dependency>
        <groupId>org.apache.commons</groupId>
        <artifactId>commons-pool2</artifactId>
        <version>2.9.0</version>
      </dependency>

      <dependency>
        <groupId>org.apache.commons</groupId>
        <artifactId>commons-lang3</artifactId>
        <version>3.11</version>
      </dependency>

      <dependency>
        <groupId>commons-io</groupId>
        <artifactId>commons-io</artifactId>
        <version>2.8.0</version>
      </dependency>

      <dependency>
        <groupId>commons-cli</groupId>
        <artifactId>commons-cli</artifactId>
        <version>1.4</version>
      </dependency>

      <dependency>
        <groupId>org.apache.commons</groupId>
        <artifactId>commons-text</artifactId>
        <version>1.9</version>
      </dependency>

      <dependency>
        <groupId>org.apache.commons</groupId>
        <artifactId>commons-math3</artifactId>
        <version>3.6.1</version>
      </dependency>

      <dependency>
        <groupId>commons-codec</groupId>
        <artifactId>commons-codec</artifactId>
        <version>1.15</version>
      </dependency>

      <dependency>
        <groupId>commons-net</groupId>
        <artifactId>commons-net</artifactId>
        <version>3.7.2</version>
      </dependency>

      <dependency>
        <groupId>org.apache.commons</groupId>
        <artifactId>commons-dbcp2</artifactId>
        <version>2.8.0</version>
      </dependency>

      <dependency>
        <groupId>org.apache.commons</groupId>
        <artifactId>commons-jexl</artifactId>
        <version>2.1.1</version>
      </dependency>

      <dependency>
        <groupId>org.apache.commons</groupId>
        <artifactId>commons-jexl3</artifactId>
        <version>3.1</version>
      </dependency>

      <dependency>
        <groupId>org.apache.httpcomponents</groupId>
        <artifactId>httpcore-nio</artifactId>
        <version>4.4.13</version>
      </dependency>

      <dependency>
        <groupId>org.apache.httpcomponents</groupId>
        <artifactId>httpasyncclient</artifactId>
        <version>4.1.4</version>
      </dependency>

      <dependency>
        <groupId>org.mozilla</groupId>
        <artifactId>rhino</artifactId>
        <version>1.7.13</version>
      </dependency>

      <dependency>
        <groupId>oro</groupId>
        <artifactId>oro</artifactId>
        <version>2.0.8</version>
      </dependency>

      <dependency>
        <groupId>org.apache.logging.log4j</groupId>
        <artifactId>log4j-core</artifactId>
        <version>2.13.3</version>
      </dependency>

      <dependency>
        <groupId>com.alibaba</groupId>
        <artifactId>fastjson</artifactId>
        <version>${fastjson.version}</version>
      </dependency>

      <dependency>
        <groupId>org.apache.httpcomponents</groupId>
        <artifactId>httpcore</artifactId>
        <version>${httpcomponents.version}</version>
      </dependency>

      <dependency>
        <groupId>org.apache.httpcomponents</groupId>
        <artifactId>httpclient</artifactId>
        <version>${httpclient.version}</version>
      </dependency>

      <dependency>
        <groupId>org.apache.httpcomponents</groupId>
        <artifactId>httpmime</artifactId>
        <version>${httpclient.version}</version>
      </dependency>

      <dependency>
        <groupId>org.quartz-scheduler</groupId>
        <artifactId>quartz</artifactId>
        <version>${quartz.version}</version>
      </dependency>

      <dependency>
        <groupId>org.apache.curator</groupId>
        <artifactId>curator-recipes</artifactId>
        <version>${curator.version}</version>
      </dependency>

      <dependency>
        <groupId>org.apache.curator</groupId>
        <artifactId>curator-framework</artifactId>
        <version>${curator.version}</version>
      </dependency>

      <dependency>
        <groupId>org.apache.curator</groupId>
        <artifactId>curator-client</artifactId>
        <version>${curator.version}</version>
        <exclusions>
          <exclusion>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
          </exclusion>
        </exclusions>
      </dependency>

      <dependency>
        <groupId>com.amazonaws</groupId>
        <artifactId>aws-java-sdk-s3</artifactId>
        <version>${amazonaws.s3}</version>
      </dependency>

      <dependency>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok</artifactId>
        <version>${lombok.version}</version>
        <scope>provided</scope>
      </dependency>

      <dependency>
        <groupId>org.hibernate</groupId>
        <artifactId>hibernate-validator</artifactId>
        <version>${validator.version}</version>
      </dependency>

      <dependency>
        <groupId>com.google.guava</groupId>
        <artifactId>guava</artifactId>
        <version>${google.guava.version}</version>
      </dependency>

      <dependency>
        <groupId>com.google.zxing</groupId>
        <artifactId>core</artifactId>
        <version>${google.zxing.version}</version>
      </dependency>

      <!--引入了Spring Boot默认的HTTP引擎Tomcat-->
      <dependency>
        <groupId>org.apache.tomcat.embed</groupId>
        <artifactId>tomcat-embed-jasper</artifactId>
        <version>${tomcat.embed.version}</version>
        <scope>provided</scope>
      </dependency>

      <!-- servlet依赖的jar包start -->
      <dependency>
        <groupId>javax.servlet</groupId>
        <artifactId>javax.servlet-api</artifactId>
        <version>4.0.1</version>
      </dependency>

      <dependency>
        <groupId>javax.servlet.jsp</groupId>
        <artifactId>javax.servlet.jsp-api</artifactId>
        <version>2.3.1</version>
      </dependency>

      <dependency>
        <groupId>org.hibernate.validator</groupId>
        <artifactId>hibernate-validator</artifactId>
        <version>${validator.version}</version>
      </dependency>

      <!--jstl标签依赖的jar包start -->
      <dependency>
        <groupId>javax.servlet</groupId>
        <artifactId>jstl</artifactId>
        <version>1.2</version>
      </dependency>

      <dependency>
        <groupId>com.github.qcloudsms</groupId>
        <artifactId>qcloudsms</artifactId>
        <version>1.0.6</version>
      </dependency>

      <dependency>
        <groupId>com.auth0</groupId>
        <artifactId>java-jwt</artifactId>
        <version>${jwt.version}</version>
      </dependency>

      <dependency>
        <groupId>org.apiguardian</groupId>
        <artifactId>apiguardian-api</artifactId>
        <version>1.1.1</version>
      </dependency>

      <dependency>
        <groupId>org.freemarker</groupId>
        <artifactId>freemarker</artifactId>
        <version>2.3.30</version>
      </dependency>

      <dependency>
        <groupId>com.thoughtworks.xstream</groupId>
        <artifactId>xstream</artifactId>
        <version>1.4.15</version>
      </dependency>

      <dependency>
        <groupId>com.miglayout</groupId>
        <artifactId>miglayout-swing</artifactId>
        <version>5.2</version>
      </dependency>

      <dependency>
        <groupId>org.apache.commons</groupId>
        <artifactId>commons-collections4</artifactId>
        <version>4.4</version>
      </dependency>

      <dependency>
        <groupId>com.fifesoft</groupId>
        <artifactId>rsyntaxtextarea</artifactId>
        <version>3.1.2</version>
      </dependency>

      <dependency>
        <groupId>com.github.weisj</groupId>
        <artifactId>darklaf-core</artifactId>
        <version>2.5.4</version>
      </dependency>

      <dependency>
        <groupId>com.github.weisj</groupId>
        <artifactId>darklaf-extensions-rsyntaxarea</artifactId>
        <version>0.3.4</version>
      </dependency>

      <dependency>
        <groupId>org.jodd</groupId>
        <artifactId>jodd-props</artifactId>
        <version>5.2.0</version>
      </dependency>

      <dependency>
        <groupId>org.jodd</groupId>
        <artifactId>jodd-log</artifactId>
        <version>5.1.6</version>
      </dependency>

      <dependency>
        <groupId>org.jodd</groupId>
        <artifactId>jodd-lagarto</artifactId>
        <version>5.1.5</version>
      </dependency>

      <dependency>
        <groupId>net.sf.saxon</groupId>
        <artifactId>Saxon-HE</artifactId>
        <version>10.3</version>
      </dependency>

      <dependency>
        <groupId>com.github.ben-manes.caffeine</groupId>
        <artifactId>caffeine</artifactId>
        <version>2.9.0</version>
      </dependency>

      <dependency>
        <groupId>com.fasterxml.jackson.core</groupId>
        <artifactId>jackson-core</artifactId>
        <version>2.12.0</version>
      </dependency>

      <dependency>
        <groupId>com.fasterxml.jackson.core</groupId>
        <artifactId>jackson-annotations</artifactId>
        <version>2.12.0</version>
      </dependency>

      <dependency>
        <groupId>com.fasterxml.jackson.core</groupId>
        <artifactId>jackson-databind</artifactId>
        <version>2.12.0</version>
      </dependency>

      <dependency>
        <groupId>org.spockframework</groupId>
        <artifactId>spock-core</artifactId>
        <version>2.0-groovy-3.0</version>
        <scope>test</scope>
      </dependency>

      <dependency>
        <groupId>bsf</groupId>
        <artifactId>bsf</artifactId>
        <version>2.4.0</version>
      </dependency>

      <dependency>
        <groupId>org.apache.tika</groupId>
        <artifactId>tika-core</artifactId>
        <version>1.26</version>
      </dependency>

      <dependency>
        <groupId>org.apache.tika</groupId>
        <artifactId>tika-parsers</artifactId>
        <version>1.26</version>
      </dependency>

      <dependency>
        <groupId>net.sf.jtidy</groupId>
        <artifactId>jtidy</artifactId>
        <version>r938</version>
      </dependency>

      <dependency>
        <groupId>org.apache.xmlgraphics</groupId>
        <artifactId>xmlgraphics-commons</artifactId>
        <version>2.6</version>
      </dependency>

      <dependency>
        <groupId>xml-apis</groupId>
        <artifactId>xml-apis</artifactId>
        <version>1.4.01</version>
      </dependency>

      <dependency>
        <groupId>xalan</groupId>
        <artifactId>xalan</artifactId>
        <version>2.7.2</version>
      </dependency>

      <dependency>
        <groupId>org.neo4j.driver</groupId>
        <artifactId>neo4j-java-driver</artifactId>
        <version>4.2.5</version>
      </dependency>

      <dependency>
        <groupId>com.helger</groupId>
        <artifactId>ph-commons</artifactId>
        <version>9.5.4</version>
      </dependency>

      <dependency>
        <groupId>com.helger</groupId>
        <artifactId>ph-css</artifactId>
        <version>6.2.4</version>
      </dependency>

      <dependency>
        <groupId>com.jayway.jsonpath</groupId>
        <artifactId>json-path</artifactId>
        <version>2.5.0</version>
      </dependency>

      <dependency>
        <groupId>org.jsoup</groupId>
        <artifactId>jsoup</artifactId>
        <version>1.13.1</version>
      </dependency>

      <dependency>
        <groupId>jcharts</groupId>
        <artifactId>jcharts</artifactId>
        <version>0.7.5</version>
      </dependency>

      <dependency>
        <groupId>javax.mail</groupId>
        <artifactId>mail</artifactId>
        <version>1.4.7</version>
      </dependency>

      <dependency>
        <groupId>io.burt</groupId>
        <artifactId>jmespath-core</artifactId>
        <version>0.5.0</version>
      </dependency>

      <dependency>
        <groupId>io.burt</groupId>
        <artifactId>jmespath-jackson</artifactId>
        <version>0.5.0</version>
      </dependency>

      <dependency>
        <groupId>net.minidev</groupId>
        <artifactId>json-smart</artifactId>
        <version>2.4.7</version>
      </dependency>

      <dependency>
        <groupId>net.minidev</groupId>
        <artifactId>accessors-smart</artifactId>
        <version>2.4.7</version>
      </dependency>

      <dependency>
        <groupId>org.beanshell</groupId>
        <artifactId>bsh-core</artifactId>
        <version>2.0b4</version>
      </dependency>

      <dependency>
        <groupId>dnsjava</groupId>
        <artifactId>dnsjava</artifactId>
        <version>3.3.1</version>
      </dependency>

      <dependency>
        <groupId>org.apache.geronimo.specs</groupId>
        <artifactId>geronimo-jms_1.1_spec</artifactId>
        <version>1.1.1</version>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.1</version>
        <configuration>
          <source>${java.version}</source>
          <target>${java.version}</target>
          <encoding>UTF-8</encoding>
        </configuration>
      </plugin>
    </plugins>
  </build>
</project>